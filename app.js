const express = require('express');
const morgan = require('morgan');
const path = require('path');
const bodyParser = require('body-parser');

const app = express();

const indexRouter = require('./routes/index');
const bicicletasRouter = require('./routes/bicicletas');
const bicicletasAPIRouter = require('./routes/api/bicicletas');

// settings
app.set('port', process.env.PORT || 3000);
app.set('views',path.join(__dirname,'views'));
app.set('view engine','ejs');

// middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(morgan('dev'));
app.use(express.static(__dirname + '/public'));

app.use('/',indexRouter); 
app.use('/bicicletas/',bicicletasRouter);  
app.use('/api/bicicletas',bicicletasAPIRouter); 
   

app.listen(app.get('port'), () => console.log(`server listening at http://localhost:${app.get('port')}`));