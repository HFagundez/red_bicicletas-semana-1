Curso:
Desarrollo del lado servidor: NodeJS, Express y MongoDB

Entrega - Semana 1

- En el proyecto usé EJS como template engine porque me resulta 
mas práctico que Pug.

- Le agregué una opción 'Lista' a la barra de navegación, a través 
de la cual se accede al listado de bicicletas y las operaciones del crud. 

- No olvidar realizar npm install para cargar los módulos.


Horacio Fagundez - hfagundez@protonmail.com