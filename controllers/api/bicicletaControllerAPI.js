var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req,res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

exports.bicicleta_create = function(req,res){
    console.log('=========> post api create recibido');
    console.log(req.body);
    console.log(req.query);
    const bici = new Bicicleta(req.body.id,req.body.color,req.body.modelo,[req.body.latitud,req.body.longitud]);
    Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_delete = function(req,res){
    console.log('=========> post api delete recibido');
    console.log(req.body);
    console.log(req.query);
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
}

exports.bicicleta_update = function(req,res){ 
    console.log('=========> post api update recibido');
    console.log('====> lat,long');
    console.log(req.body.ubicacion[0]);
    console.log(req.body.ubicacion[1]);
    let bici = Bicicleta.findById(req.body.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.ubicacion[0], req.body.ubicacion[1]];

    res.status(200).json({
        bicicleta: bici
    });
}