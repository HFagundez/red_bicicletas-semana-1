var Bicicleta = require('../models/bicicleta');

exports.getIndex = (req, res) => {
    console.log('root accesed in route');
    
    res.render('index',{title:"Bicicletas"});
    
    }

exports.bicicleta_list = (req, res) => {
    console.log(Bicicleta.allBicis);
    
    res.render('bicicletas/index',{bicis:Bicicleta.allBicis});
    }

    
    exports.bicicleta_create_get = function(req,res){ 
        console.log('get create recibido')      
        res.render('bicicletas/create');
    }
    
    exports.bicicleta_create_post = function(req,res){ 
        console.log('post create recibido');
        const bici = new Bicicleta(req.body.id,req.body.color,req.body.modelo,[req.body.latitud,req.body.longitud]);
        Bicicleta.add(bici);
        res.redirect('/bicicletas');
    }
    
    exports.bicicleta_update_get = function(req,res){ 
        console.log('get update recibido');
        let bici = Bicicleta.findById(req.params.id);
        console.log('Bicicleta a modificar');
        console.log(bici);
        res.render('bicicletas/update',{bici});
    }
    
    exports.bicicleta_update_post = function(req,res){ 
        console.log('post update recibido');
        console.log('====> lat,long');
        console.log(req.body.latitud);
        console.log(req.body.longitud);
        let bici = Bicicleta.findById(req.params.id);
        bici.id = req.body.id;
        bici.color = req.body.color;
        bici.modelo = req.body.modelo;
        bici.ubicacion = [req.body.latitud, req.body.longitud];
    
        res.redirect('/bicicletas');
    }
    
    exports.bicicleta_delete_post = function(req,res){ 
        console.log('post delete recibido');        
        Bicicleta.removeById(req.body.id);
        res.redirect('/bicicletas');
    }    