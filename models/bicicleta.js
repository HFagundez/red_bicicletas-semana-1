var Bicicleta = function (id,color,modelo,ubicacion) {
    this.id=id,
    this.color=color,
    this.modelo=modelo,
    this.ubicacion=ubicacion
}

/* Bicicleta.prototype.toString = function() {
    return `id: ${this.id} | color: ${this.color} | modelo: ${this.modelo}`
} */

Bicicleta.allBicis = [];

Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function (aBiciId) {
    console.log('Contenido Tabla Bicis:');
    console.log(Bicicleta.allBicis);
    let aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici;
    else    
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);            
}

Bicicleta.removeById = function (aBiciId) {
    //Bicicleta.findById(aBiciId);
    for (let i=0; i<Bicicleta.allBicis.length;i++){
        if (Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }    
}

const a = new Bicicleta(1256,'Rojo','Urbana',[40.759279, -73.985226]);
const b = new Bicicleta(2398,'Blanco y Azul','Montaña',[40.758465, -73.985295]);
const c = new Bicicleta(2398,'Amarillo y Verde','Bmx Pro',[40.762440, -73.994741]);

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);

module.exports=Bicicleta;